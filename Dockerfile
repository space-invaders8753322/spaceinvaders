FROM node:19-alpine as node
RUN mkdir /vue && npm i -g npm@latest
WORKDIR /vue
STOPSIGNAL SIGHUP
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh && ln -s /entrypoint.sh /usr/local/bin/entrypoint
ENTRYPOINT [ "entrypoint" ]
