export default interface Cargo {
  capacity: number;
  units: number;
  inventory: any[];
}
