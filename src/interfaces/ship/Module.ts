export default interface Module {
  symbol: string;
  name: string;
  description: string;
  capacity?: number;
  range?: number;
  deposits?: string[];
  requirements: {
    crew: number;
    power: number;
    slots: number;
  };
}
