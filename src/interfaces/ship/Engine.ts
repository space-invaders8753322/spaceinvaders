export default interface Engine {
    symbol: string;
    name: string;
    description: string;
    speed: number;
    requirements: {
      power: number;
      crew: number;
    };
}
