import Engine from "./Engine";
import Frame from "./Frame";
import Reactor from "./Reactor";
import Mount from "./Mount";
import Module from "./Module";

export default interface PurchasableShip {
    type: string;
    name: string;
    description: string;
    purchasePrice: number;
    frame: Frame;
    reactor: Reactor;
    engine: Engine;
    modules: Module[];
    mounts: Mount[];
}
