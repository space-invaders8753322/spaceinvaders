export default interface ConsumedFuel {
  amount: number;
  timestamp: string;
}
