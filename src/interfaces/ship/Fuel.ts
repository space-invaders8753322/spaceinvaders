import type ConsumedFuel from "./ConsumedFuel";

export default interface Fuel {
  current: number;
  capacity: number;
  consumed: ConsumedFuel;
}
