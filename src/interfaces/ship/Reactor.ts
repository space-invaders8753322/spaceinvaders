export default interface Reactor {
    symbol: string;
    name: string;
    description: string;
    powerOutput: number;
    requirements: {
        crew: number;
    };
}
