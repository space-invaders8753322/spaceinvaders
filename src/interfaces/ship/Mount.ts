export default interface Mount {
  symbol: string;
  name: string;
  description: string;
  strength: number;
  deposits?: string[];
  requirements: {
    crew: number;
    power: number;
  };
}
