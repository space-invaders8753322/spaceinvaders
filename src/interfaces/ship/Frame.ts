export default interface Frame {
    symbol: string;
    name: string;
    description: string;
    moduleSlots: number;
    mountingPoints: number;
    fuelCapacity: number;
    requirements: {
        power: number;
        crew: number;
    }
}
