import type Nav from "../navigation/Nav";
import type Crew from "./Crew";
import type Engine from "./Engine";
import type Frame from "./Frame";
import type Fuel from "./Fuel";
import type Reactor from "./Reactor";
import type Mount from "./Mount";
import type Module from "./Module";
import type Registration from "./Registration";
import type Cargo from "./Cargo";

export default interface Ship {
  symbol: string;
  nav: Nav;
  crew: Crew;
  fuel: Fuel;
  frame: Frame;
  reactor: Reactor;
  engine: Engine;
  modules: Module[];
  mounts: Mount[];
  registration: Registration;
  cargo: Cargo;
}
