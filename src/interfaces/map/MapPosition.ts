import MapItem from "./MapItem";

export default interface MapPosition 
{
  x: number,
  y: number,
  type: string,
  items: MapItem[],
  owned: boolean
}
