import Trait from "../Trait";
import type Waypoint from "../navigation/Waypoint";
import Action from "./Action";

export default interface MapItem
{
  name: string,
  type: string,
  owned: boolean
  waypoint: Waypoint,
  traits: Trait[],
  actions: Action[],
}
