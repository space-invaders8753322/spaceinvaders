export default interface Action
{
  name: string
  act(...args: any[]): any
}
