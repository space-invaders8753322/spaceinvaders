import Transaction from "./Transaction";
import Ship from "./ship/PurchasableShip";

export default interface Shipyard {
    symbol: string;
    shipTypes: {
        type: string;
    }[];
    transactions: Transaction[];
    ships: Ship[];
}
