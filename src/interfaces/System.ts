import SystemFaction from "@/interfaces/SystemFaction";
import SystemWaypoint from "@/interfaces/SystemWaypoint";

export default interface System {
    symbol: string;
    sectorSymbol: string;
    type: string;
    x: number;
    y: number;
    waypoints: SystemWaypoint[];
    factions: SystemFaction[];
}