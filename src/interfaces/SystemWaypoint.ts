export default interface SystemWaypoint {
    symbol: string;
    type: string;
    x: number;
    y: number;
}