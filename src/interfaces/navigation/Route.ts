import type Waypoint from "./Waypoint";

export default interface Route {
  departure: Waypoint;
  destination: Waypoint;
  arrival: string;
  departureTime: string;
}
