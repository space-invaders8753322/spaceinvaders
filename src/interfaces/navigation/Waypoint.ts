import type Trait from '../Trait';

export default interface Waypoint {
  systemSymbol: string;
  symbol: string;
  type: string;
  x: number;
  y: number;
  orbitals?: {
    symbol: string;
  }[];
  traits?: Trait[];
  chart?: {
    submittedBy: string;
    submittedOn: string;
  }
  faction?: {
    symbol: string;
  }
}
