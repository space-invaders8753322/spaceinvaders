export default interface Ownable {
  owned: boolean
}
