import { createApp } from 'vue';
import store from './store';
import App from './App.vue';
import axios from './plugins/customAxios';
import VueAxios from 'vue-axios';
import router from './router';
import vuetify from './plugins/vuetify';
import { loadFonts } from './plugins/webfontloader';

loadFonts()

createApp(App)
  .use(router)
  .use(vuetify)
  .use(store)
  .use(VueAxios, axios)
  .mount('#app')

store.commit('updateCompanyData');
