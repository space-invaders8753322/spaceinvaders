import type Waypoint from "../interfaces/navigation/Waypoint";
import type MapPosition from "@/interfaces/map/MapPosition";
import type MapItem from "@/interfaces/map/MapItem";
import type Action from "@/interfaces/map/Action";
import type { Router } from "vue-router";

export default class MapService {
  public static getIcons(): Record<string, string> {
    return {
      PLANET: "mdi-earth",
      MOON: "mdi-moon-waning-crescent",
      ASTEROID_FIELD: "mdi-asterisk",
      GAS_GIANT: "mdi-cloud-circle",
      ORBITAL_STATION: "mdi-orbit",
      JUMP_GATE: "mdi-gate-or",
      MULTIPLE: "mdi-checkbox-multiple-blank-circle",
    };
  }

  public static createMapEntry(
    waypoint: Waypoint,
    owned: boolean = false
  ): MapItem {
    let mapEntry = {
      name: waypoint.symbol,
      type: waypoint.type,
      waypoint: waypoint,
      traits: waypoint.traits ?? [],
      actions: [] as Action[],
      owned: owned,
    };

    mapEntry.traits.forEach((trait) => {
      if (trait.name == "Shipyard") {
        mapEntry.actions.push({
          name: "buy ships",
          act: (...args: any[]) => {
            const waypoint = args[0] as Waypoint;
            const router = args[1] as Router;

            router.push(
              `/systems/${waypoint.systemSymbol}/shipyard/${waypoint.symbol}`
            );
          },
        });
      }
    });

    return mapEntry;
  }

  public static getExistingEntry(
    map: MapPosition[],
    waypoint: Waypoint
  ): MapPosition | undefined {
    return map.find((mapEntry) => {
      return mapEntry.x === waypoint.x && mapEntry.y === waypoint.y;
    });
  }

  public static getMapIcon(type: string): string {
    return this.getIcons()[type] ?? "mdi-help";
  }

  public static createMapPosition(entry: MapItem, owned: boolean): MapPosition {
    return {
      x: entry.waypoint.x,
      y: entry.waypoint.y,
      type: entry.type,
      items: [entry],
      owned: owned,
    };
  }
}
