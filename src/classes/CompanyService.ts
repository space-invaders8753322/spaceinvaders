import type Ship from '@/interfaces/ship/Ship';
import axios from '../plugins/customAxios'

export default class CompanyService
{
    static async updateCompanyData()
    {
        const response = await axios.get('my/agent');

        return response.data.data;
    }

    static async getShips(system?: string)
    {
        const response = await axios.get('my/ships');

        const ships = response.data.data as Ship[];

        if (system === undefined) {
          return ships;
        }

        return ships.filter(ship => ship.nav.route.departure.systemSymbol === system);
    }
}
