import axios from '../plugins/customAxios';

export default class Universe
{
    static async getSystemWaypoints(system: string) {
        const response = await axios.get(`systems/${system}/waypoints/`);

        return response.data.data;
    }

    static async getSystems(page: number, limit: number = 20) {
        const response = await axios.get(`systems/?page=${page}&limit=${limit}`);
        return response.data;
    }
}
