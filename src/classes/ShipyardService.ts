import axios from '../plugins/customAxios';
import type Shipyard from '@/interfaces/Shipyard'

export default class ShipyardService
{
    static async getShipyard(system: string, waypoint: string): Promise<Shipyard> {
        const response = await axios.get(`systems/${system}/waypoints/${waypoint}/shipyard`);

        return response.data.data;
    }

    static async getAvailableShips(system: string, waypoint: string) {
        const shipyard = await this.getShipyard(system, waypoint);

        return shipyard.ships;
    }

    static async buyShip(shipyard: Shipyard, shipType: string) {
        return await axios.post('my/ships', {
            shipType: shipType,
            waypointSymbol: shipyard.symbol
        })
    }
}
