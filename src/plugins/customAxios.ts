import axios from "axios";
import store from "../store";

axios.defaults.baseURL = import.meta.env.VITE_URL
axios.defaults.headers.common['Authorization'] = 'Bearer ' + import.meta.env.VITE_TOKEN;
axios.defaults.headers.post['Content-Type'] = 'application/json'

axios.interceptors.response.use(response => {
    if (response.config.method === 'post') {
        store.commit('updateCompanyData');
    }

    return response;
});

export default axios;
