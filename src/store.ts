import { createStore } from 'vuex';
import CompanyService from './classes/CompanyService';

const store = createStore({
    state () {
        return {
            companyData: {}
        }
    },
    mutations: {
        async updateCompanyData(state) {
            state.companyData = await CompanyService.updateCompanyData();
        } 
    }
})

export default store;
