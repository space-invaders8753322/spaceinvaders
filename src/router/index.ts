import { createRouter, createWebHistory, RouteRecordRaw } from "vue-router";
import HomeView from "../views/HomeView.vue";
import AboutView from "../views/AboutView.vue";
import SystemView from "../views/SystemView.vue";
import SystemsView from "../views/SystemsView.vue";
import ShipyardView from "../views/ShipyardView.vue";

const routes: Array<RouteRecordRaw> = [
  {
    path: "/",
    name: "home",
    component: HomeView,
  },
  {
    path: "/systems",
    name: "systems",
    component: SystemsView,
  },
  {
    path: "/systems/:id",
    name: ":id",
    component: SystemView,
  },
  {
    path: "/systems/:systemId/shipyard/:waypointId",
    name: "Shipyard at :waypointId",
    component: ShipyardView,
  },
  {
    path: "/about",
    name: "about",
    component: AboutView,
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

export default router;
